A Free Software, minimalistic and privacy friendly email app.

'''Highlights'''
* Easy navigation
* No unnecessary settings
* No bells and whistles
* Privacy friendly
* Simple design

'''Why?'''

The focus of SimpleEmail is be a privacy-friendly email app with a good UX/UI and with a community development model.

SimpleEmail is minimalistic by design, so you can concentrate on reading and writing messages, it starts a foreground 
service with a low priority status bar notification to make sure you'll never miss a new email.

This project has been forked from [[eu.faircode.email]].

'''Features'''

* 100% Free Software
* Multiple accounts (inboxes)
* Multiple identities (outboxes)
* Unified inbox
* Conversation view
* Two way synchronization
* Offline storage and operations
* Battery friendly
* Low data usage
* Folder management
* Signatures
* Dark theme
* Account colors
* Search on server
* Encryption/decryption
* Export settings

'''Secure:'''
* Allow encrypted connections only
* Accept valid security certificates only
* Authentication required
* Safe message view (styling, scripting and unsafe HTML removed)
* No special permissions required
* No advertisements
* No analytics and no tracking
